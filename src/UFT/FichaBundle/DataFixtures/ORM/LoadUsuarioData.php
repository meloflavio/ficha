<?php

namespace UFT\FichaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use UFT\FichaBundle\DataFixtures\ORM\LoadFichaBundleData;
use UFT\UserBundle\Entity\Usuario;

/**
 * Description of LoadUsuarioData
 *
 * @author flaviomelo
 */
class LoadUsuarioData extends LoadFichaBundleData implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {

        $campins = $this->getModelFixtures();

        foreach ($campins['Usuario'] as $reference => $columns) {
            $userAdmin = new Usuario();
            $userAdmin->setUsername($columns['username']);
            $userAdmin->setPlainPassword($columns['plainpassword']);
            $userAdmin->setEmail($columns['email']);
            $userAdmin->addRole($columns['roles']);
            $userAdmin->setEnabled(true);
            $manager->persist($userAdmin);
            $manager->flush();
        }
    }

    public function getOrder() {
        return 7;
    }

    public function getModelFile() {
        return 'usuario';
    }

}
