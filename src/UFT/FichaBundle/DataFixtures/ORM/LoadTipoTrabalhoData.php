<?php

namespace UFT\FichaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use UFT\FichaBundle\DataFixtures\ORM\LoadFichaBundleData;
use UFT\FichaBundle\Entity\TipoTrabalho;

/**
 * Description of LoadTipoTrabalhoData
 *
 * @author flaviomelo
 */
class LoadTipoTrabalhoData extends LoadFichaBundleData implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {

        $campins = $this->getModelFixtures();

        foreach ($campins['TipoTrabalho'] as $reference => $columns) {
            $model = new TipoTrabalho();
            $model->setDescricao($columns['descricao']);
            $manager->persist($model);
            $manager->flush();
            $this->addReference( $reference, $model);
        }
    }

    public function getOrder() {
        return 1;
    }

    public function getModelFile() {
        return 'tipotrabalho';
    }

}
