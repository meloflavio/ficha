<?php

namespace UFT\FichaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use UFT\FichaBundle\DataFixtures\ORM\LoadFichaBundleData;
use UFT\FichaBundle\Entity\Cutter;

/**
 * Description of LoadCutterData
 *
 * @author flaviomelo
 */
class LoadCutter2Data extends LoadFichaBundleData implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {

        $campins = $this->getModelFixtures();

        foreach ($campins['Cutter'] as $reference => $columns) {
            $model = new Cutter();
            $model->setLetra($columns['letra']);
            $model->setCodigo($columns['codigo']);
            $model->setDescricao($columns['descricao']);
            $manager->persist($model);
            $manager->flush();
        }

      
    }

    public function getOrder() {
        return 6;
    }

    public function getModelFile() {
        return 'cutter2';
    }

}
