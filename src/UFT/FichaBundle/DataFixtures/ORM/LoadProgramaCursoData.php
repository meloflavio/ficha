<?php

namespace UFT\FichaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use UFT\FichaBundle\DataFixtures\ORM\LoadFichaBundleData;
use UFT\FichaBundle\Entity\ProgramaCurso;

/**
 * Description of LoadProgramaCursoData
 *
 * @author flaviomelo
 */
class LoadProgramaCursoData extends LoadFichaBundleData implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {

        $campins = $this->getModelFixtures();

        foreach ($campins['ProgramaCurso'] as $reference => $columns) {
            $model = new ProgramaCurso();
            $model->setDescricao($columns['descricao']);
            $model->setCdd($columns['cdd']);
            $tipoPrograma = $this->getReference('tipoprogramacurso_' . $columns['tipoPrograma']);
            $model->setTipoPrograma($tipoPrograma);
            $manager->persist($model);
            $manager->flush();
            $this->addReference($reference, $model);
        }
    }

    public function getOrder() {
        return 3;
    }

    public function getModelFile() {
        return 'programacurso';
    }

}
