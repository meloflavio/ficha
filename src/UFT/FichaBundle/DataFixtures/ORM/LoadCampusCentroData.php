<?php

namespace UFT\FichaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use UFT\FichaBundle\DataFixtures\ORM\LoadFichaBundleData;
use UFT\FichaBundle\Entity\CampusCentro;

/**
 * Description of LoadCampusCentroData
 *
 * @author flaviomelo
 */
class LoadCampusCentroData extends LoadFichaBundleData implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {

        $campins = $this->getModelFixtures();

        foreach ($campins['CampusCentro'] as $reference => $columns) {
            $campus = new CampusCentro();
            $campus->setNome($columns['nome']);
            foreach ($columns['cursos'] as $reference => $valor) {
                $curso = $this->getReference($reference);
                $campus->addProgramacurso($curso);
            }
            $manager->persist($campus);
            $manager->flush();
        }
    }

    public function getOrder() {
        return 4;
    }

    public function getModelFile() {
        return 'campuscentro';
    }

}
