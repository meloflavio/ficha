<?php

namespace UFT\FichaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use UFT\FichaBundle\DataFixtures\ORM\LoadFichaBundleData;
use UFT\FichaBundle\Entity\TipoProgramaCurso;

/**
 * Description of LoadTipoProgramaCursoData
 *
 * @author flaviomelo
 */
class LoadTipoProgramaCursoData extends LoadFichaBundleData implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {

        $campins = $this->getModelFixtures();

        foreach ($campins['TipoProgramaCurso'] as $reference => $columns) {
            $model = new TipoProgramaCurso();
            $model->setDescricao($columns['descricao']);
            $tipoTrabalho = $this->getReference('tipotrabalho_' . $columns['tipoTrabalho']);
            $model->setTipoTrabalho($tipoTrabalho);
            $manager->persist($model);
            $manager->flush();
            $this->addReference($reference, $model);
        }
    }

    public function getOrder() {
        return 2;
    }

    public function getModelFile() {
        return 'tipoprogramacurso';
    }

}
