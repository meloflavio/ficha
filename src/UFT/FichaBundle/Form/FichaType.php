<?php

/**
 * Created by PhpStorm.
 * User: carlosalves
 * Date: 04/11/14
 * Time: 09:54
 */

namespace UFT\FichaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class FichaType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('nomeAutor', 'text'); //NOME DO AUTOR

        $builder->add('sobrenomeAutor', 'text'); // ULTIMO NOME DO AUTOR

        $builder->add('tituloTrabalho', 'text'); //TITULO DO TRABALHO

        $builder->add('subtituloTrabalho', 'text', array('required' => false)); //SUBTITULO DO TRABALHO

        $builder->add('tipoTrabalho'); //TIPO DO TRABALHO

        $builder->add('campus'); // CAMPUS OU CENTRO

        $builder->add('programa'); // PROGRAMA OU CURSO

        $builder->add('nomeOrientador', 'text'); //NOME DO ORIENTADOR

        $builder->add('sobrenomeOrientador', 'text'); // SOBRENOME DO ORIENTADOR

        $builder->add('orientadora', 'checkbox', array('empty_data' => false, 'required' => false)); // CHECKBOX CASO ORIENTADOR SEJA MULHER

        $builder->add('nomeCoorientador', 'text', array('required' => false)); // NOME DO COORIENTADOR

        $builder->add('sobrenomeCoorientador', 'text', array('required' => false)); // SOBRENOME DO COORIENTADOR

        $builder->add('coorientadora', 'checkbox', array('required' => false)); // CHECKBOX CASO COORIENTADORA SEJA MULHER

        $builder->add('ano', 'date', array('format' => 'dd-MM-yyyy', 'required' => true,'data' => new \DateTime("now"))); // ANO DO TRABALHO

        $builder->add('numPaginas', null); // NUMERO DE PAGINAS DO TRABALHO

        $builder->add('assuntos', 'collection', array('type' => 'text', 'required' => true)); // ASSUNTOS DO TRABALHO - PALAVRAS CHAVES

        $builder->add('tipoImpressao', 'choice', array('choices' => array('PDF' => 'PDF', 'Imagem' => 'Imagem'), 'expanded' => true, 'multiple' => false, 'required' => true)); // radio tipo Impressao
        
        $builder->add('tipoFonte', 'choice', array('choices' => array('Arial' => 'arial', 'Times New Roman' => 'times new roman'), 'expanded' => true, 'multiple' => false, 'required' => true)); // radio tipo Impressao
    }

    public function getName() {
        return 'formulario';
    }

}
