<?php

namespace UFT\FichaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use UFT\FichaBundle\Entity\TipoTrabalho;
use UFT\FichaBundle\Form\TipoTrabalhoType;

/**
 * TipoTrabalho controller.
 *
 */
class TipoTrabalhoController extends Controller
{

    /**
     * Lists all TipoTrabalho entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UFTFichaBundle:TipoTrabalho')->findAll();

        return $this->render('UFTFichaBundle:TipoTrabalho:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TipoTrabalho entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TipoTrabalho();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tipotrabalho_show', array('id' => $entity->getId())));
        }

        return $this->render('UFTFichaBundle:TipoTrabalho:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a TipoTrabalho entity.
     *
     * @param TipoTrabalho $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoTrabalho $entity)
    {
        $form = $this->createForm(new TipoTrabalhoType(), $entity, array(
            'action' => $this->generateUrl('tipotrabalho_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TipoTrabalho entity.
     *
     */
    public function newAction()
    {
        $entity = new TipoTrabalho();
        $form   = $this->createCreateForm($entity);

        return $this->render('UFTFichaBundle:TipoTrabalho:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TipoTrabalho entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UFTFichaBundle:TipoTrabalho')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoTrabalho entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('UFTFichaBundle:TipoTrabalho:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TipoTrabalho entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UFTFichaBundle:TipoTrabalho')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoTrabalho entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('UFTFichaBundle:TipoTrabalho:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a TipoTrabalho entity.
    *
    * @param TipoTrabalho $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoTrabalho $entity)
    {
        $form = $this->createForm(new TipoTrabalhoType(), $entity, array(
            'action' => $this->generateUrl('tipotrabalho_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing TipoTrabalho entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UFTFichaBundle:TipoTrabalho')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoTrabalho entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

//            return $this->redirect($this->generateUrl('tipotrabalho_edit', array('id' => $id)));
            return $this->redirect($this->generateUrl('tipotrabalho_show', array('id' => $id)));
        }

        return $this->render('UFTFichaBundle:TipoTrabalho:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TipoTrabalho entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('UFTFichaBundle:TipoTrabalho')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoTrabalho entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tipotrabalho'));
    }

    /**
     * Creates a form to delete a TipoTrabalho entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipotrabalho_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
