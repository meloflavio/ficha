<?php

namespace UFT\FichaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller {

    public function indexAction() {
        return $this->render('UFTFichaBundle:Default:index.html.twig');
    }

}
