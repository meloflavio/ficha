<?php

namespace UFT\FichaBundle\Controller;

use UFT\FichaBundle\Form\FichaType;
use UFT\FichaBundle\Entity\Ficha;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class FichaController extends Controller {

    private $ficha;

    public function fichaAction() {
        $this->ficha = new Ficha();

//     $repository = $this->container->get('doctrine.orm.entity_manager')->getRepository('UFTFichaBundle:CampusCentro');

        $form = $this->createForm(new FichaType($this->getDoctrine()->getManager()), $this->ficha);

        $request = $this->get('request');


        if ($request->isMethod('POST')) {

            $form->handleRequest($request);

            if ($form->isValid()) {

                print_r($form->getData());
                die();

                return $this->redirect($this->generateUrl('projeto_ficha_ficha'));
            }
        }

        return $this->render('UFTFichaBundle:Ficha:ficha.html.twig', array('form' => $form->createView()));
    }

    public function novaAction() {
        $request = $this->get('request');
        $ficha = new Ficha($request->request->get('formulario'));
        //campus

        $centroRepository = $this->getDoctrine()
                ->getRepository('UFTFichaBundle:CampusCentro');
        $campus = $centroRepository->findOneById($ficha->getCampus());
        $ficha->setCampus($campus);
        //Programa
        $programaRepository = $this->getDoctrine()
                ->getRepository('UFTFichaBundle:ProgramaCurso');
        $programa = $programaRepository->findOneById($ficha->getPrograma());
        $ficha->setPrograma($programa);
        //Tipo Curso
        $tipoTrabalhoRepository = $this->getDoctrine()
                ->getRepository('UFTFichaBundle:TipoTrabalho');
        $tipoTrabalho = $tipoTrabalhoRepository->findOneById($ficha->getTipoTrabalho());
        $ficha->setTipoTrabalho($tipoTrabalho);

        $cutter = $this->getCutter($ficha->getSobrenomeAutor(), $ficha->getTituloTrabalho());
//        var_dump($ficha->getTipoFonte());
//        die();
//        

        if ($ficha->getTipoImpressao() == 'PDF') {
            $html = $this->renderView('UFTFichaBundle:Ficha:modelo_pdf.html.twig', array('ficha' => $ficha, 'cutter' => $cutter, 'margin_left' => "90"));
            return new Response(
                    $this->get('knp_snappy.pdf')->getOutputFromHtml($html), 200, array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="file.pdf"'
                    )
            );
        } else {
            $html = $this->renderView('UFTFichaBundle:Ficha:modelo_img.html.twig', array('ficha' => $ficha, 'cutter' => $cutter, 'margin_left' => "150"));

//            $this->get('knp_snappy.image')->generateFromHtml(
//                $html
//                ,
//                $this->get('kernel')->getRootDir() . '/../web'.'/download/file2333.jpg',array('quality'=>100,'width'=>'512'),true
//            );
//            $file = $this->get('kernel')->getRootDir() . '/../web'.'/download/file2333.jpg';
//            $response = new BinaryFileResponse($file);
//            return $response;
            return new Response(
                    $this->get('knp_snappy.image')->getOutputFromHtml($html), 200, array(
                'Content-Type' => 'image/jpg',
                'Content-Disposition' => 'attachment; filename="image.jpg"'
                    )
            );
        }

//        return $this->render('UFTFichaBundle:Ficha:modelo_img.html.twig', array('ficha' => $ficha, 'cutter' => $cutter));
    }

    public function ajaxAction(Request $request) {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }

        $id = $request->query->get('campus_id');
        $result = array();

        $programaRepository = $this->getDoctrine()
                ->getRepository('UFTFichaBundle:ProgramaCurso');
        $qb = $programaRepository->createQueryBuilder('p')
                ->join('p.campuscentros', 'c')
                ->where('c.id = :id')
                ->orderBy('p.descricao', 'ASC')
                ->setParameter('id', $id);
        $programa = $qb->getQuery()->getResult();

//        $result['Selecione o Programa/Curso'] = 0;
        foreach ($programa as $curso) {
            $result[$curso->getDescricao()] = $curso->getId();
        }

        return new JsonResponse($result);
    }

    public function ajaxprogramaAction(Request $request) {
//        if (!$request->isXmlHttpRequest()) {
//            throw new NotFoundHttpException();
//        }

        $id = $request->query->get('programa_id');
        $result = array();

        $programaRepository = $this->getDoctrine()
                ->getRepository('UFTFichaBundle:ProgramaCurso');
        $qb = $programaRepository->createQueryBuilder('p')
                ->select('p', 'tp', 'tt')
                ->leftJoin('p.tipoPrograma', 'tp')
                ->leftJoin('tp.tipoTrabalho', 'tt')
                ->where('p.id = :id')
                ->orderBy('tt.descricao', 'ASC')
                ->setParameter('id', $id);
        $programa = $qb->getQuery()->getArrayResult();
        $tipoPrograma = $programa[0]['tipoPrograma']['tipoTrabalho'];

//        $result['Selecione o Tipo de Trabalho'] = 0;
        $result[$tipoPrograma['descricao']] = $tipoPrograma['id'];

        return new JsonResponse($result);
    }

    public function getCutter($name, $titulo) {
        //Removendo pontos,virgulas e espaços --- Regra MC, M' e MAC
        $prefix = array("MC", "M'", "M’");
        $pontuacao = array(".", ",", " ");
        $nameUpper = strtoupper($name);
        $nameUpper = str_replace($pontuacao, "", $nameUpper);
        $nameUpper = str_replace($prefix, "MAC", $nameUpper);

        //Select codigo cutter (Se as primeiras letras do nome não ocorrerem na tabela, tomem-se as próximas anteriores na ordem alfabética)
        $query = $this->getDoctrine()->getManager()
                        ->createQuery(
                                'SELECT c FROM UFTFichaBundle:Cutter c
                        WHERE c.descricao = (SELECT max(a.descricao) 
                            FROM UFTFichaBundle:Cutter a
                            WHERE a.descricao <= :nome)'
                        )->setParameter('nome', $nameUpper);

        $cutter = $query->getSingleResult();

        //Remove artigos
        $artigos = array("a", "as", "o", "os", "um", "uma", "uns", "umas");
        $numeros = array('1', '2', '3', '4', '5', '6', '7', '8', '9');
        $numerosExtenso = array('um', 'dois', 'treis', 'quatro', 'cinco', 'seis', 'sete', 'oito', 'nove');
        $tituloSemArtigo = preg_replace('/\b(' . implode('|', $artigos) . ')\b/', '', strtolower($titulo));
        $tituloSemArtigo = str_replace($numeros, $numerosExtenso, $tituloSemArtigo);

        try {
            //retorna primeira letra do sobre nome + codigo cutter + primeira letra da primeira palavra do titulo que não seja artigo
            return (substr($nameUpper, 0, 1) . $cutter->getCodigo() . substr(trim($tituloSemArtigo), 0, 1));
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

}
