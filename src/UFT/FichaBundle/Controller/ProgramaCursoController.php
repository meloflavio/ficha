<?php

namespace UFT\FichaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use UFT\FichaBundle\Entity\ProgramaCurso;
use UFT\FichaBundle\Form\ProgramaCursoType;

/**
 * ProgramaCurso controller.
 *
 */
class ProgramaCursoController extends Controller
{
    private $programa;
    /**
     * Lists all ProgramaCurso entities.
     *
     */
    public function indexAction()
    {
        $this->programa = new ProgramaCurso();
        $form = $this->createForm(new ProgramaCursoType($this->getDoctrine()->getManager()), $this->programa);
        $request = $this->get('request');
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
        }


        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UFTFichaBundle:ProgramaCurso')->findAll();

        return $this->render('UFTFichaBundle:ProgramaCurso:index.html.twig', array(
            'entities' => $entities, 'form' => $form->createView(),

        ));
    }
    /**
     * Creates a new ProgramaCurso entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new ProgramaCurso();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('programacurso_show', array('id' => $entity->getId())));
        }

        return $this->render('UFTFichaBundle:ProgramaCurso:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a ProgramaCurso entity.
     *
     * @param ProgramaCurso $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ProgramaCurso $entity)
    {
        $form = $this->createForm(new ProgramaCursoType(), $entity, array(
            'action' => $this->generateUrl('programacurso_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new ProgramaCurso entity.
     *
     */
    public function newAction()
    {
        $entity = new ProgramaCurso();
        $form   = $this->createCreateForm($entity);

        return $this->render('UFTFichaBundle:ProgramaCurso:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ProgramaCurso entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UFTFichaBundle:ProgramaCurso')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProgramaCurso entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('UFTFichaBundle:ProgramaCurso:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ProgramaCurso entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UFTFichaBundle:ProgramaCurso')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProgramaCurso entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('UFTFichaBundle:ProgramaCurso:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }





    /**
    * Creates a form to edit a ProgramaCurso entity.
    *
    * @param ProgramaCurso $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ProgramaCurso $entity)
    {
        $form = $this->createForm(new ProgramaCursoType(), $entity, array(
            'action' => $this->generateUrl('programacurso_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }




    /**
     * Edits an existing ProgramaCurso entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UFTFichaBundle:ProgramaCurso')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProgramaCurso entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('programacurso_show', array('id' => $id)));
        }

        return $this->render('UFTFichaBundle:ProgramaCurso:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a ProgramaCurso entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('UFTFichaBundle:ProgramaCurso')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ProgramaCurso entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('programacurso'));
    }

    /**
     * Creates a form to delete a ProgramaCurso entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('programacurso_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete','attr' => array('class'=> 'btn btn-default')))
            ->getForm()
        ;
    }
}
