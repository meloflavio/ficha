<?php

namespace UFT\FichaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use UFT\FichaBundle\Entity\CampusCentro;
use UFT\FichaBundle\Form\CampusCentroType;

/**
 * CampusCentro controller.
 */
class CampusCentroController extends Controller
{

   /**
     * 
     * @Template("UFTFichaBundle:CampusCentro:index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UFTFichaBundle:CampusCentro')->findAll();

//        return $this->render('UFTFichaBundle:CampusCentro:index.html.twig', array('entities' => $entities));
        return array(
            'entities' => $entities,
        );
    }
    /**
     * 
     * @Template("UFTFichaBundle:CampusCentro:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new CampusCentro();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('campus_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a CampusCentro entity.
     *
     * @param CampusCentro $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CampusCentro $entity)
    {
        $form = $this->createForm(new CampusCentroType(), $entity, array(
            'action' => $this->generateUrl('campus_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Salvar'));

        return $form;
    }

    /**
     * 
     *
     * @Template()
     */
    public function newAction()
    {
        $entity = new CampusCentro();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a list CampusCentro entity.
     *
     * @Template("UFTFichaBundle:CampusCentro:show.html.twig")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

//        $entity = $em->getRepository('UFTFichaBundle:CampusCentro')->find($id);
        $campusRepository = $this->getDoctrine()
                ->getRepository('UFTFichaBundle:CampusCentro');
        $qb = $campusRepository->createQueryBuilder('c')
                ->select('c,p')
                ->leftJoin('c.programacursos', 'p')
                ->where('c.id = :id')
                ->orderBy('c.nome', 'ASC')
                ->setParameter('id', $id);
        $entity = $qb->getQuery()->getArrayResult();
       

        if (!$entity) {
            throw $this->createNotFoundException('Não foi possivel encontrar o campus selecionado.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity[0],
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing CampusCentro entity.
     *
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UFTFichaBundle:CampusCentro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Não foi possivel encontrar o campus selecionado.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a CampusCentro entity.
    *
    * @param CampusCentro $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CampusCentro $entity)
    {
        $form = $this->createForm(new CampusCentroType(), $entity, array(
            'action' => $this->generateUrl('campus_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing CampusCentro entity.
     *
     * @Method("PUT")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UFTFichaBundle:CampusCentro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Não foi possivel encontrar o campus selecionado.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('campus_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView()
            
        );
    }
    /**
     * Deletes a CampusCentro entity.
     *
     * 
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);
        if ($form->isValid()) {        

            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('UFTFichaBundle:CampusCentro')->find($id);
            
            if (!$entity) {
                throw $this->createNotFoundException('Não foi possivel encontrar o campus selecionado.');
            }

            $em->remove($entity);
            $em->flush();
        }
        return $this->redirect($this->generateUrl('campus'));
    }

    /**
     * Creates a form to delete a CampusCentro entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('campus_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Deletar'))
            ->getForm()
        ;
    }
}
