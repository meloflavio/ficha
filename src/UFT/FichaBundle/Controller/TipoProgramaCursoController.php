<?php

namespace UFT\FichaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use UFT\FichaBundle\Entity\TipoProgramaCurso;
use UFT\FichaBundle\Form\TipoProgramaCursoType;

/**
 * TipoProgramaCurso controller.
 *
 */
class TipoProgramaCursoController extends Controller
{

    /**
     * Lists all TipoProgramaCurso entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UFTFichaBundle:TipoProgramaCurso')->findAll();

        return $this->render('UFTFichaBundle:TipoProgramaCurso:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TipoProgramaCurso entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TipoProgramaCurso();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tipoprogramacurso_show', array('id' => $entity->getId())));
        }

        return $this->render('UFTFichaBundle:TipoProgramaCurso:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a TipoProgramaCurso entity.
     *
     * @param TipoProgramaCurso $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoProgramaCurso $entity)
    {
        $form = $this->createForm(new TipoProgramaCursoType(), $entity, array(
            'action' => $this->generateUrl('tipoprogramacurso_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TipoProgramaCurso entity.
     *
     */
    public function newAction()
    {
        $entity = new TipoProgramaCurso();
        $form   = $this->createCreateForm($entity);

        return $this->render('UFTFichaBundle:TipoProgramaCurso:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TipoProgramaCurso entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UFTFichaBundle:TipoProgramaCurso')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoProgramaCurso entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('UFTFichaBundle:TipoProgramaCurso:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TipoProgramaCurso entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UFTFichaBundle:TipoProgramaCurso')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoProgramaCurso entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('UFTFichaBundle:TipoProgramaCurso:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a TipoProgramaCurso entity.
    *
    * @param TipoProgramaCurso $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoProgramaCurso $entity)
    {
        $form = $this->createForm(new TipoProgramaCursoType(), $entity, array(
            'action' => $this->generateUrl('tipoprogramacurso_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing TipoProgramaCurso entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UFTFichaBundle:TipoProgramaCurso')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoProgramaCurso entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('tipoprogramacurso_show', array('id' => $id)));
        }

        return $this->render('UFTFichaBundle:TipoProgramaCurso:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TipoProgramaCurso entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('UFTFichaBundle:TipoProgramaCurso')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoProgramaCurso entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tipoprogramacurso'));
    }

    /**
     * Creates a form to delete a TipoProgramaCurso entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipoprogramacurso_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
