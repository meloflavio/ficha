<?php

namespace UFT\FichaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CampusCentro
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class CampusCentro
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255)
     */
    private $nome;
    
    
    /**
     * @ORM\ManyToMany(targetEntity="ProgramaCurso", inversedBy="campuscentros" , cascade={"refresh","persist","merge"})
     * @ORM\JoinTable(name="ProgramaCursosCampus",
     *      joinColumns={@ORM\JoinColumn(name="campuscentro_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="programacurso_id", referencedColumnName="id")}
     *      )
     */
    private $programacursos;


    function __construct() {
        
        $this->programacursos = new \Doctrine\Common\Collections\ArrayCollection();
        
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return CampusCentro
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }
    
    /**
     * Get programacursos
     * 
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    function getProgramacursos() {
        return $this->programacursos;
    }

    function addProgramacursos(ProgramaCurso $programacurso) {
        $this->programacursos[] = $programacurso;
    }

    
    public function __toString()
    {
    return $this->nome;
    }


    /**
     * Add programacursos
     *
     * @param \UFT\FichaBundle\Entity\ProgramaCurso $programacursos
     * @return CampusCentro
     */
    public function addProgramacurso(\UFT\FichaBundle\Entity\ProgramaCurso $programacursos)
    {
        $this->programacursos[] = $programacursos;

        return $this;
    }

    /**
     * Remove programacursos
     *
     * @param \UFT\FichaBundle\Entity\ProgramaCurso $programacursos
     */
    public function removeProgramacurso(\UFT\FichaBundle\Entity\ProgramaCurso $programacursos)
    {
        $this->programacursos->removeElement($programacursos);
    }
}
