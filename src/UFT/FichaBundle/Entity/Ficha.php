<?php

namespace UFT\FichaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Ficha
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Ficha {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomeAutor", type="string", length=150)
     */
    private $nomeAutor;

    /**
     * @var string
     *
     * @ORM\Column(name="sobrenomeAutor", type="string", length=80)
     */
    private $sobrenomeAutor;

    /**
     * @var string
     *
     * @ORM\Column(name="tituloTrabalho", type="string", length=255)
     */
    private $tituloTrabalho;

    /**
     * @var string
     *
     * @ORM\Column(name="subtituloTrabalho", type="string", length=255)
     */
    private $subtituloTrabalho;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="ProgramaCurso")
     * @ORM\JoinColumn(name="id_programa", referencedColumnName="id")
     */
    private $programa;

    /**
     * @ORM\ManyToOne(targetEntity="TipoTrabalho")
     * @ORM\JoinColumn(name="id_tipoTrabalho", referencedColumnName="id")
     */
    private $tipoTrabalho;

    /**
     * @ORM\ManyToOne(targetEntity="CampusCentro")
     * @ORM\JoinColumn(name="id_campus", referencedColumnName="id")
     */
    private $campus;

    /**
     * @var string
     *
     * @ORM\Column(name="nomeOrientador", type="string", length=255)
     */
    private $nomeOrientador;

    /**
     * @var string
     *
     * @ORM\Column(name="sobrenomeOrientador", type="string", length=255)
     */
    private $sobrenomeOrientador;

    /**
     * @var string
     *
     * @ORM\Column(name="nomeCoorientador", type="string", length=255)
     */
    private $nomeCoorientador;

    /**
     * @var string
     *
     * @ORM\Column(name="sobrenomeCoorientador", type="string", length=255)
     */
    private $sobrenomeCoorientador;

    /**
     * @var date
     *
     * @ORM\Column(name="ano", type="date")
     */
    private $ano;

    /**
     * @var integer
     * @Assert\Regex(pattern= "/^[0-9]\d*$/",message= "This text cannot contain numbers" )
     * @ORM\Column(name="numPaginas", type="decimal")
     */
    private $numPaginas;

    /**
     * @var boolean
     *
     * @ORM\Column(name="orientadora", type="boolean")
     */
    private $orientadora;

    /**
     * @var boolean
     *
     * @ORM\Column(name="coorientadora", type="boolean")
     */
    private $coorientadora;
    private $assuntos;
    private $tipoImpressao;
    private $tipoFonte;

//#######################----------------------GETTERS AND SETTERS--------------------------###########################

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nomeAutor
     *
     * @param string $nomeAutor
     * @return Ficha
     */
    public function setNomeAutor($nomeAutor) {
        $this->nomeAutor = $nomeAutor;

        return $this;
    }

    /**
     * Get nomeAutor
     *
     * @return string
     */
    public function getNomeAutor() {
        return $this->nomeAutor;
    }

    /**
     * Set sobrenomeAutor
     *
     * @param string $sobrenomeAutor
     * @return Ficha
     */
    public function setSobrenomeAutor($sobrenomeAutor) {
        $this->sobrenomeAutor = $sobrenomeAutor;

        return $this;
    }

    /**
     * Get sobrenomeAutor
     *
     * @return string
     */
    public function getSobrenomeAutor() {
        return $this->sobrenomeAutor;
    }

    /**
     * Set tituloTrabalho
     *
     * @param string $tituloTrabalho
     * @return Ficha
     */
    public function setTituloTrabalho($tituloTrabalho) {
        $this->tituloTrabalho = $tituloTrabalho;

        return $this;
    }

    /**
     * Get tituloTrabalho
     *
     * @return string
     */
    public function getTituloTrabalho() {
        return $this->tituloTrabalho;
    }

    /**
     * Set subtituloTrabalho
     *
     * @param string $subtituloTrabalho
     * @return Ficha
     */
    public function setSubtituloTrabalho($subtituloTrabalho) {
        $this->subtituloTrabalho = $subtituloTrabalho;

        return $this;
    }

    /**
     * Get subtituloTrabalho
     *
     * @return string
     */
    public function getSubtituloTrabalho() {
        return $this->subtituloTrabalho;
    }

    /**
     * Set programa
     *
     * @param integer $programa
     * @return Ficha
     */
    public function setPrograma($programa) {
        $this->programa = $programa;

        return $this;
    }

    /**
     * Get programa
     *
     * @return integer
     */
    public function getPrograma() {
        return $this->programa;
    }

    /**
     * Get tipoTrabalho
     *
     * @return integer
     */
    public function getTipoTrabalho() {
        return $this->tipoTrabalho;
    }

    /**
     * Set programa
     *
     * @param integer $tipoTrabalho
     * @return Ficha
     */
    public function setTipoTrabalho($tipoTrabalho) {
        $this->tipoTrabalho = $tipoTrabalho;
    }

    /**
     * Set campus
     *
     * @param integer $campus
     * @return Ficha
     */
    public function setCampus($campus) {
        $this->campus = $campus;

        return $this;
    }

    /**
     * Get campus
     *
     * @return integer
     */
    public function getCampus() {
        return $this->campus;
    }

    /**
     * Set nomeOrientador
     *
     * @param string $nomeOrientador
     * @return Ficha
     */
    public function setNomeOrientador($nomeOrientador) {
        $this->nomeOrientador = $nomeOrientador;

        return $this;
    }

    /**
     * Get nomeOrientador
     *
     * @return string
     */
    public function getNomeOrientador() {
        return $this->nomeOrientador;
    }

    /**
     * Set sobrenomeOrientador
     *
     * @param string $sobrenomeOrientador
     * @return Ficha
     */
    public function setSobrenomeOrientador($sobrenomeOrientador) {
        $this->sobrenomeOrientador = $sobrenomeOrientador;

        return $this;
    }

    /**
     * Get sobrenomeOrientador
     *
     * @return string
     */
    public function getSobrenomeOrientador() {
        return $this->sobrenomeOrientador;
    }

    /**
     * Get orientadora
     *
     * @return boolean
     */
    public function isOrientadora() {
        return $this->orientadora;
    }

    /**
     * @param boolean $orientadora
     */
    public function setOrientadora($orientadora) {
        $this->orientadora = $orientadora;
    }

    /**
     * Set nomeCoorientador
     *
     * @param string $nomeCoorientador
     * @return Ficha
     */
    public function setNomeCoorientador($nomeCoorientador) {
        $this->nomeCoorientador = $nomeCoorientador;

        return $this;
    }

    /**
     * Get nomeCoorientador
     *
     * @return string
     */
    public function getNomeCoorientador() {
        return $this->nomeCoorientador;
    }

    /**
     * Get sobrenomeCoorientador
     *
     * @return string
     */
    public function getSobrenomeCoorientador() {
        return $this->sobrenomeCoorientador;
    }

    /**
     * Set sobrenomeCoorientador
     *
     * @param string $sobrenomeCoorientador
     * @return Ficha
     */
    public function setSobrenomeCoorientador($sobrenomeCoorientador) {
        $this->sobrenomeCoorientador = $sobrenomeCoorientador;

        return $this;
    }

    /**
     * Get coorientadora
     *
     * @return boolean
     */
    public function isCoorientadora() {
        return $this->coorientadora;
    }

    /**
     * @param boolean $coorientadora
     */
    public function setCoorientadora($coorientadora) {
        $this->coorientadora = $coorientadora;
    }

    /**
     * Set ano
     *
     * @param date $ano
     * @return Ficha
     */
    public function setAno($ano) {
        $this->ano = $ano;

        return $this;
    }

    /**
     * Get ano
     *
     * @return date
     */
    public function getAno() {
        return $this->ano;
    }

    /**
     * Set numPaginas
     *
     * @param integer $numPaginas
     * @return Ficha
     */
    public function setNumPaginas($numPaginas) {
        $this->numPaginas = $numPaginas;

        return $this;
    }

    /**
     * Get numPaginas
     *
     * @return integer
     */
    public function getNumPaginas() {
        return $this->numPaginas;
    }

    /**
     * @return mixed
     */
    public function getAssuntos() {
        if ($this->assuntos == null)
            return array('1' => '', '2' => '', '3' => '', '4' => '');
        else
            return $this->assuntos;
    }

    /**
     * @param mixed $assuntos
     */
    public function setAssuntos($assuntos) {
        $this->assuntos = $assuntos;
    }

    function __construct($array = null) {
        if ($array != null) {
            $this->nomeAutor = $array['nomeAutor'];
            $this->sobrenomeAutor = $array['sobrenomeAutor'];
            $this->tituloTrabalho = $array['tituloTrabalho'];
            $this->subtituloTrabalho = $array['subtituloTrabalho'];
            $this->programa = $array['programa'];
            $this->tipoTrabalho = $array['tipoTrabalho'];
            $this->campus = $array['campus'];
            $this->nomeOrientador = $array['nomeOrientador'];
            $this->sobrenomeOrientador = $array['sobrenomeOrientador'];
            $this->nomeCoorientador = $array['nomeCoorientador'];
            $this->sobrenomeCoorientador = $array['sobrenomeCoorientador'];
            $this->ano = $array['ano'];
            $this->numPaginas = $array['numPaginas'];
            if (!isset($array['orientadora']))
                $array['orientadora'] = false;
            $this->orientadora = $array['orientadora'];
            if (!isset($array['coorientadora']))
                $array['coorientadora'] = false;
            $this->coorientadora = $array['coorientadora'];
            $this->tipoImpressao = $array['tipoImpressao'];
            $this->assuntos = $array['assuntos'];
            $this->tipoFonte = $array['tipoFonte'];
        }
    }

    /**
     * Get orientadora
     *
     * @return boolean 
     */
    public function getOrientadora() {
        return $this->orientadora;
    }

    /**
     * Get coorientadora
     *
     * @return boolean 
     */
    public function getCoorientadora() {
        return $this->coorientadora;
    }

    function getTipoImpressao() {
        return $this->tipoImpressao;
    }

    function setTipoImpressao($tipoImpressao) {
        $this->tipoImpressao = $tipoImpressao;
    }

    function getTipoFonte() {
        return $this->tipoFonte;
    }

    function setTipoFonte($tipoFonte) {
        $this->tipoFonte = $tipoFonte;
    }

}
