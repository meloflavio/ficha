<?php

namespace UFT\FichaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProgramaCurso
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ProgramaCurso {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=255)
     */
    private $descricao;

    /**
     * @ORM\ManyToOne(targetEntity="TipoProgramaCurso")
     * @ORM\JoinColumn(name="id_tipoPrograma", referencedColumnName="id")
     */
    private $tipoPrograma;

    
     /**
     * @ORM\ManyToMany(targetEntity="CampusCentro", mappedBy="programacursos" , cascade={"persist"})
     * @ORM\JoinTable(name="ProgramaCursosCampus",
     *      joinColumns={@ORM\JoinColumn(name="programacurso_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="campuscentro_id", referencedColumnName="id")}
     *      )
     */
    private $campuscentros;
    
    /**
     * @var string
     *
     * @ORM\Column(name="cdd", type="string", length=10)
     */
    private $cdd;

    function __construct() {
        $this->campus = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     * @return ProgramaCurso
     */
    public function setDescricao($descricao) {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string 
     */
    public function getDescricao() {
        return $this->descricao;
    }

    public function __toString() {
        return $this->descricao;
    }

    /**
     * Set tipoPrograma
     *
     * @param integer $tipoPrograma
     * 
     * @return ProgramaCurso
     */
    public function setTipoPrograma($tipoPrograma) {
        $this->tipoPrograma = $tipoPrograma;
    }

    /**
     * Get tipoPrograma
     *
     * @return integer
     */
    public function getTipoPrograma() {
        return $this->tipoPrograma;
    }

    /**
     * Get programacursos
     * 
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    function getCampuscentros() {
        return $this->campuscentros;
    }

    function setCampuscentros(CampusCentro $campuscentro) {
        $this->campus[] = $campuscentro;
    }



    /**
     * Add campuscentros
     *
     * @param \UFT\FichaBundle\Entity\CampusCentro $campuscentros
     * @return ProgramaCurso
     */
    public function addCampuscentro(\UFT\FichaBundle\Entity\CampusCentro $campuscentros)
    {
        $this->campuscentros[] = $campuscentros;

        return $this;
    }

    /**
     * Remove campuscentros
     *
     * @param \UFT\FichaBundle\Entity\CampusCentro $campuscentros
     */
    public function removeCampuscentro(\UFT\FichaBundle\Entity\CampusCentro $campuscentros)
    {
        $this->campuscentros->removeElement($campuscentros);
    }

    /**
     * Set cdd
     *
     * @param string $cdd
     * @return ProgramaCurso
     */
    public function setCdd($cdd)
    {
        $this->cdd = $cdd;

        return $this;
    }

    /**
     * Get cdd
     *
     * @return string 
     */
    public function getCdd()
    {
        return $this->cdd;
    }
}
