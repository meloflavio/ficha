<?php

namespace UFT\FichaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoProgramaCurso
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class TipoProgramaCurso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=150)
     */
    private $descricao;


    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="TipoTrabalho")
     * @ORM\JoinColumn(name="id_tipoTrabalho", referencedColumnName="id")
     */
    private $tipoTrabalho;





    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     * @return TipoProgramaCurso
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string 
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Get tipoTrabalho
     *
     * @return int
     */
    public function getTipoTrabalho()
    {
        return $this->tipoTrabalho;
    }

    /**
     * Set tipoTrabalho
     *
     * @param integer $tipoTrabalho
     * @return TipoProgramaCurso
     */
    public function setTipoTrabalho($tipoTrabalho)
    {
        $this->tipoTrabalho = $tipoTrabalho;
    }



    public function __toString()
    {
        return $this->descricao;
    }





}
