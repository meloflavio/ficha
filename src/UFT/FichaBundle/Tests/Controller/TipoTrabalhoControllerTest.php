<?php

namespace UFT\FichaBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TipoTrabalhoControllerTest extends WebTestCase
{
    
    public function testCompleteScenario()
    {
        // Create a new client to browse the application
        $client = static::createClient();

        // Create a new entry in the database
        $crawler = $client->request('GET', '/tipotrabalho/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /tipotrabalho/");
        $crawler = $client->click($crawler->selectLink('Novo tipo de trabalho')->link());

        // Fill in the form and submit it
        $form = $crawler->selectButton('Salvar')->form(array(
            'projeto_bundle_fichabundle_tipotrabalho[descricao]'  => 'Test',
            // ... other fields to fill
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check data in the show view
        $this->assertGreaterThan(0, $crawler->filter('td:contains("Test")')->count(), 'Missing element td:contains("Test")');

        // Edit the entity
        $crawler = $client->click($crawler->selectLink('Editar')->link());

        $form = $crawler->selectButton('Alterar')->form(array(
            'projeto_bundle_fichabundle_tipotrabalho[descricao]'  => 'Foo',
            // ... other fields to fill
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check the element contains an attribute with value equals "Foo"
        $this->assertGreaterThan(0, $crawler->filter('td:contains("Foo")')->count(), 'Missing element [value="Foo"]');

        // Delete the entity
         $crawler = $client->click($crawler->selectLink('Editar')->link());
        $client->submit($crawler->selectButton('Deletar')->form());
        $crawler = $client->followRedirect();

        // Check the entity has been delete on the list
        $this->assertNotRegExp('/Foo/', $client->getResponse()->getContent());
    }

    
}
