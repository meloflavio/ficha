<?php

namespace UFT\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
/**
 * Description of UFTUserBundle
 *
 * @author flaviomelo
 */
class UFTUserBundle extends Bundle {

     public function getParent()
    {
        return 'FOSUserBundle';
    }
   
}